package com.oreillyauto.dao.impl;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.CarpartsRepositoryCustom;
import com.oreillyauto.domain.Carpart;
import com.oreillyauto.domain.QCarpart;

@Repository
public class CarpartsRepositoryImpl extends QuerydslRepositorySupport implements CarpartsRepositoryCustom {
	
	QCarpart carpartTable = QCarpart.carpart;
	
	public CarpartsRepositoryImpl() {
		super(Carpart.class);
	}

	// Get Carpart By Part Number Version 1
	@Override
	public Carpart getCarpartByPartNumber(String partNumber) {
		return null;
	}
	
	// Get Carpart By Part Number Version 2
//	@Override
//	public Carpart getCarpartByPartNumber(String partNumber) {
//		return from(carpartTable)
//				.where(carpartTable.partNumber.equalsIgnoreCase(partNumber))
//				.limit(1)
//				.fetchOne();
//	}
	
}

