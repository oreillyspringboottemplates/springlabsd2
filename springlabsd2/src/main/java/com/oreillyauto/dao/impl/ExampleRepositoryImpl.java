package com.oreillyauto.dao.impl;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.ExampleRepositoryCustom;
import com.oreillyauto.domain.Example;
import com.oreillyauto.domain.QExample;

@Repository
public class ExampleRepositoryImpl extends QuerydslRepositorySupport implements ExampleRepositoryCustom {
	
	QExample exampleTable = QExample.example;
			
	public ExampleRepositoryImpl() {
		super(Example.class);
	}
	
}
