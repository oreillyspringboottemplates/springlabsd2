package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.CarpartsRepositoryCustom;
import com.oreillyauto.domain.Carpart;

public interface CarpartsRepository extends CrudRepository<Carpart, String>, CarpartsRepositoryCustom {

}

