package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springlabsd2Application {

	public static void main(String[] args) {
		SpringApplication.run(Springlabsd2Application.class, args);
	}

}
