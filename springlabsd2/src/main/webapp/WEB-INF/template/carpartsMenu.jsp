<%@ include file="/WEB-INF/layouts/include.jsp"%>
<nav class="nav flex-column">
	<a class="${active eq 'H4666ST' ? 'active nav-link' : 'nav-link'}"
		href="<c:url value='/carparts/electrical/H4666ST'/>">Headlight	Bulb</a> 
	<a class="${active eq 'M134HV' ? 'active nav-link' : 'nav-link'}"
		href="<c:url value='/carparts/engine/M134HV' />">Oil Pumps</a>
	<a class="${active eq '40026' ? 'active nav-link' : 'nav-link'}"
		href="<c:url value='/carparts/other/40026' />">Air Pump</a>
</nav>